# GitLab DevSecOps Playbook - Version 0.1a - May 2023

> Label: WIP - Work In Progress  
**[Maturity Level](https://about.gitlab.com/direction/maturity/) : `maturity: minimal`**

This playbook is designed to help you implement effective DevSecOps practices using GitLab in your company, no matter how big or small with a holistic approach.  
Our goal is to provide clear instructions and practical actionable steps for introducing security controls, measuring their effectiveness, and showing your business leaders the value of these practices.
Security mechanisms must be pervasive, simple, scalable, and easy to manage.

By following this playbook, your teams can create applications that are significantly more secure, which is ultimately what we aim for.  
Each step will have a label associated with it, indicating:

1. level of recommendation as Priority 1 to 10 with 1 being a Must and 10 being optional.
2. grade of complexity and diffculty to implement from easy to Medium to hard.
3. the minimum subscription this feature would need to be implemented: free, Premium or Ultimate

The list will be structured in 4 parts designated to:

1. **Best Practice** - generally accepted as superior to other known alternatives
   1. Refers to a method or technique that has been consistently proven through research, experience, and expert consensus to be the most effective and efficient way of achieving a particular goal or solving a problem. Best practices are generally widely adopted and standardized across industries and organizations as a benchmark for excellence.
2. **Good Practice** - has worked exceptionally well
   1. Refers to a method or technique that is generally accepted as effective and efficient, but may not have the same level of evidence-based support as a best practice. Good practices are typically developed through trial and error, expert opinions, and experience, and are often used as a starting point for further improvement.
3. **Emergent Practice** - proofed to be helpful in many cases
   1. Refers to a method or technique that is still in the experimental stage, and has not yet been fully validated or standardized. Emergent practices are often developed in response to emerging trends or new challenges, and are constantly evolving as new information becomes available.
4. **Novel Practice** - might be a fitting solution for some issues
   1. Refers to a completely new or innovative method or technique that has not yet been widely tested or adopted. Novel practices may be developed in response to completely new challenges or opportunities, or may be the result of disruptive technologies or other paradigm shifts. While novel practices may hold great promise, they also carry a higher degree of uncertainty and risk.


## Ideas:

- [ ] (@Sam) Interactive Form to choose for the customer what he has and then limit the choices they have in the list of Bets Practises.
  - SaaS - SM, Size of Installation , 
  To reduce inofrmation overload

  1st iteration List uniteractive with just Prio 1 Best Practises and then define out of this
  Define Basic Parameters for the Customer to different and what they have adopted.

  Export as an issue Template they can import in to their collaboration project or own instance (Automation later with Feedback channel to CSM what they have done and implemented, similar to the secuirty config in the project)
