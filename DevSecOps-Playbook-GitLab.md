# GitLab DevSecOps Playbook

## Best Practises

| Name | Description | Prio | Diff | Sub |
| ---- | ----------- | ---- | ---- | --- |
| [Source Code Versioning/Management](#sourcecode-management) | Version Control Systems (VCS) introduce an auditable history, and Compliance with granular access controls and reporting |  1 | easy | Free |
| [Enforce 2FA](#enforce-2fa) | Two-factor authentication (2FA) provides an additional level of security to your users’ GitLab account. |  1 | Easy | Free |
| [Top Level/Root Group](#toplevel-or-root-group) | Transfer all top-groups under one root group | 1 | Medium | Free |
| [Group project policy enforcement](#group-project-policy-enforcement) | Define a project in your root group whose settings will be applied to all projects in the group | 1 | Medium | Free |
| Instance-level default branch protection | Enforcing Merge Request Workflow on protected branches |  1 | Easy | Free |
| [Disabling http(s) git actions](#disabling-http-git-actions) | Use the SSH protocol to access your repositories instead of HTTPS |   2 | Medium | Free |
| [Push rules](https://docs.gitlab.com/ee/user/project/repository/push_rules.html) | Push rules or pre-receive Git hooks give you more control over what can and can’t be pushed to your repository. |  1 | Easy | Premium |
| [Git Server Hooks](https://docs.gitlab.com/ee/administration/server_hooks.html) | run custom logic on the GitLab server |  4 | Medium | Free |
| [Commit Signing](#commit-signing) | Sign all commits to verify that the author is genuine |  2 | Medium | Free |
| [Limit the lifetime of SSH keys](#limit-the-lifetime-of-ssh-keys) | enforce more protection by requiring the regular rotation of these keys |  2 | Easy | Ultimate |
| [Dedicated AppSec Group](#dedicated-appsec-group) | Create a group with the members of your AppSec Engineers in the Root Group | 1 | Easy | Free |
| [Policy Project](#policy-project) | Define a project in your group whose settings will be applied to all projects in the group | 3 |  Medium |
| [Bulk edit projects](#bulk-edit-projects) | Bulk update a number of projects or all projects in a group. Write a report of what changes. | 5 | Medium |
| [.gitignore](#gitignore) | .gitignore files help prevent accidental commits of sensitive, debug, or workstation specific data | 1 | easy |  |
| [Code Owners](#code-owners) | Create a CODEOWNERS file in the repository that identifies people and teams that own specific parts of the repository and should be consulted via MR when those parts of the repo are modified. | 1 | Easy | Premium |
| [Merge request approval rules](#merge-request-approval-rules) | Description | Prio | Diff |
| [SECURITY.md](#security-markdown-file-as-security-policy) | Create a SECURITY.md file in your repository that explains who to contact if you find a security issue in the application | 1 | Easy | Free |
| [CI/CD Pipeline](#cicd-pipeline) | Implement a CI/CD pipeline | 1 | Medium | Free |
| [Application Environments](#application-environments) | Create separate environments for dev, staging and prod, and treat each as independent with its own data, testing and requirements | 2 | Medium | Free |
| [Application Data Separation](#application-data-separation) | Make sure that dev and test environments are not using the same data as production. If the use of live data is required then make sure that data is anonymized. | 3 | Medium | Free |
| [CI/CD Administration](#cicd-administration) | Create and enforce user or team roles so that only the appropriate people can change or disable tests and deployment requirements | 3 | Medium | Free |
| [CI/CD Templates](#cicd-templates) | Curate a repository of CI/CD RTemplates for the critical Jobs in your Pipelines | 3 | Hard | Free |
| [Remote Development Enviroment](#remote-development-enviroment) | 4 | Medium | Free |
| [IDE plugins](#ide-plugins) | Most IDE's support the use of third-party plugins, and devs should implement these tools to highlight security issues as they happen in realtime while they are programming. | 2 | Easy | Free |
| [Local Software Composition Analysis](#local-software-composition-analysis) | Helps you find and fix libraries with known security issues | 3 |  Medium |
| [Local Static Code Analysis](#local-static-code-analysis) | Helps you find and fix security vulnerabilities in your source code | 2 | Easy |
| [Local Sensitive Data Analysis](#local-sensitive-data-analysis) | Audits your repository for secrets, credentials, API keys and similar in dev environment. Secrets stored in source code are visible to other people | 2 | Easy |

### SourceCode Management

Source code management is utilised to track modifications to a source code repository, maintain history of changes to a code base and helps resolve conflicts when merging updates from multiple contributors.  
The use of git repositories as a distributed source code management has become the defacto solution for most organisations, rather than traditional centralised source code version control solutions.  
Public and private Git repositories will have different considerations to security posture, and require separated security policies and configuration.  
Private repositories may still be access by outsourced suppliers, business partners or temporary developers. SCM and primary GIT repositories may be sync or replicated to external locations for partners to cached versions of designated repositories.  
SCM ensures the integrity and non-repudiation for any source code changes (incl commits, push, pull, branches or merges).

1. Never store secrets in code or configuration.
1. Scan code for secrets and credentials during code commits
1. Enable multi-factor authentication for contributors when available
1. Validate any client or 3rd party applications requesting access
1. Add security testing in your CI/CD pipelines to detect vulnerabilities
1. Regularly rotate credentials such as SSH keys and access tokens used by developers.
1. Maintain clearly separated trust boundaries and access policies between Public and Private GIT repositories

### Enforce 2FA

[Two-factor authentication (2FA)](https://docs.gitlab.com/ee/security/two_factor_authentication.html) provides an additional level of security to your users’ GitLab account.

### Toplevel or root Group

[prevent-new-users-from-creating-top-level-groups](https://docs.gitlab.com/ee/administration/user_settings.html#use-configuration-files-to-prevent-new-users-from-creating-top-level-groups)

### Group project policy enforcement

[Group project policy enforcement](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/group-project-policy-enforcement) minimizes the attack surfscer to your projects and gives you ensured security postured Topp-Down.  
Define a project in your root group whose settings will be applied to all projects in the group

### Disabling http git actions

[Disabling http(s) git actions](https://docs.gitlab.com/ee/user/admin_area/settings/visibility_and_access_controls.html#configure-enabled-git-access-protocols) and uise the SSH protocol to access your repositories instead of HTTPS

### Push rules

[Push rules](https://docs.gitlab.com/ee/user/project/repository/push_rules.html) are [pre-receive Git hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) you can enable in a user-friendly interface. Push rules give you more control over what can and can’t be pushed to your repository. Specific rules might be:

- [Prevent pushing secrets to the repository](https://docs.gitlab.com/ee/user/project/repository/push_rules.html#prevent-pushing-secrets-to-the-repository).
- Evaluating the contents of a commit.
- Confirming commit messages match expected formats.
- Enforcing branch name rules.
- Evaluating the details of files.
- Preventing Git tag removal.

### Git Server Hooks

[Git Server Hooks](https://docs.gitlab.com/ee/administration/server_hooks.html) allow you to run custom logic on the GitLab server. You can use them to run Git-related tasks such as:

- Enforcing specific commit policies.
- Performing tasks based on the state of the repository.

### Commit Signing

Commit Signing with [GPG](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/index.html) or [SSH](https://docs.gitlab.com/ee/user/project/repository/ssh_signed_commits/) verifies that the author of  commit is genuine.

### Limit the lifetime of SSH keys

[Limiting the lifetime of SSH keys](https://docs.gitlab.com/ee/user/admin_area/settings/account_and_limit_settings.html#limit-the-lifetime-of-ssh-keys) enforces more protection by requiring the regular rotation of these keys.

### Dedicated AppSec Group

Create a group with the members of your AppSec Engineers in the [Root/Top level Group](#toplevel-or-root-group). Locate all Security Policies, Compliance Ci Filoes and other Secuirty resources in here. Use the group with your App Sec Engineers as Members to enable them as Approval Givers on Security Blocked Merge Request without the need to give each member seperately those permissions and duties.

### Policy Project

Define a [policy project](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/group-project-policy-enforcement) in your group whose settings will be applied to all projects in the group

### Bulk edit projects

[Bulk update](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/bulk-edit-projects) a number of projects or all projects in a group. Write a report of what changes.

### .gitignore

`.gitignore` files help prevent accidental commits of sensitive, debug, or workstation specific

### Code Owners

[Code Owners](https://docs.gitlab.com/ee/user/project/code_owners.html) and [Merge request approval rules](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html) | Create a CODEOWNERS file in the repository that identifies people and teams that own specific parts of the repository and should be consulted via MR when those parts of the repo are modified.
Here is an example [Codeowners file](https://docs.gitlab.com/ee/user/project/code_owners.html#example-codeowners-file).

### Merge request approval rules

[Merge request approval rules](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html)

### SECURITY.md

Create a SECURITY.md file in your repository that explains who to contact if you find a security issue in the application.

### CI/CD Pipeline

Implement a CI/CD pipeline

### Application Environments

Create separate environments for dev, staging and prod, and treat each as independent with its own data, testing and requirements.

### Application Data Separation

Make sure that dev and test environments are not using the same data as production. If the use of live data is required then make sure that data is anonymized.

### CI/CD Administration

Create and enforce user or team roles so that only the appropriate people can change or disable tests and deployment requirements

### CI/CD Templates

Curate a repository of CI/CD Templates for the critical Jobs in your Pipelines.

### Security Markdown File as Security Policy

Adding a security policy to your repository you can give instructions for how to report a security vulnerability in your project by adding a security policy to your repository. Add information about supported versions of your project and how to report vulnerabilities.

### Remote Development Enviroment

[A Remote Development ENviroment](https://docs.gitlab.com/ee/user/project/remote_development/) connected to the [Web Ide](https://docs.gitlab.com/ee/user/project/web_ide_beta/index.html)

### IDE plugins

Most IDE's support the use of third-party plugins, and devs should implement these tools to highlight security issues as they happen in realtime while they are programming.

### Local Software Composition Analysis

Helps you find and fix libraries with known security issues.

### Local Static Code Analysis

Helps you find and fix security vulnerabilities in your source code.

### Local Sensitive Data Analysis

Audits your repository for secrets, credentials, API keys and similar in dev environment. Secrets stored in source code are visible to other people
